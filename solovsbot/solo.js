var p4 =
    {
        l : 6,
        c : 7,
        tab : [],
        turn : 1,
        coups: 0,
        game_status : 1,
        score : [0,0],
        scoreJ1 : document.createElement('p'),
        scoreJ2 : document.createElement('p'),
        box : document.createElement('div'),
        span : document.createElement('span'),

        init : function(parent, lignes, colonnes)
        {
            if(lignes) this.l = lignes;
            if(colonnes) this.c = colonnes;

            var t = document.createElement('table');

            for(var i = this.l-1; i >= 0; i--)
            {
                var tr = document.createElement('tr');
                this.tab[i] = [];
                for(var j = 0; j < this.c; j++)
                {
                    var td = document.createElement('td');
                    td.dataset.colonne = j;
                    tr.appendChild(td);
                    this.tab[i][j] = td;
                }
                t.appendChild(tr);
            }
            parent.innerHTML = '';
            parent.appendChild(t);

            t.addEventListener('click', function(e){p4.clic(e)});

            this.box.id = 'box';
            this.box.style.display = 'none';
            var btn = document.createElement('button');
            btn.textContent = 'Recommencer';

            btn.addEventListener('click', function(){p4.reset()});

            this.box.appendChild(this.span);
            this.box.appendChild(btn);
            parent.appendChild(this.box);

            var score = document.createElement('div');
            score.style.display = 'flex';
            score.style.justifyContent = 'space-between';

            this.scoreJ1.textContent = 'J1 : 0';
            score.appendChild(this.scoreJ1);

            this.scoreJ2.textContent = 'J2 : 0';
            this.scoreJ2.style.textAlign = 'right';
            score.appendChild(this.scoreJ2);

            parent.appendChild(score);

        },

        clic : function(event)
        {
            if(p4.game_status === 1 && p4.turn === 1)
            {
                var column = event.target.dataset.colonne;
                if(column)
                {
                    p4.play(parseInt(column));
                    if(p4.game_status === 1 && p4.turn === 2)
                        p4.bot();
                }
            }
        },

        set : function(row, column, player)
        {
            this.tab[row][column].className = 'J' + player;
            this.turn = 3 - this.turn;
        },

        play : function(column)
        {
            var row;
            for(i = 0; i < this.l; i++)
                if(!this.tab[i][column].className)
                {
                    row = i;
                    break;
                }

            if(row === undefined)
            {
                if(this.turn === 2)
                    this.bot();
                else
                    this.turn === 1;
                return;
            }
            this.set(row, column, this.turn);
            this.coups++;
            var test = this.win(row, column, 'J' + (3 - this.turn));
            if(test)
            {
                for(var i = 0; i < test.length; i++)
                    test[i].classList.add('winner');

                this.score[3-this.turn-1]++;
                this.span.textContent = 'J' + (3 - this.turn) + ' remporte la partie';
                if(3-this.turn === 1)
                    this.scoreJ1.textContent = 'J1 : '+this.score[3-this.turn-1];
                else
                    this.scoreJ2.textContent = 'J2 : '+this.score[3-this.turn-1];
                this.game_status = 0;
                if(this.turn === 2)
                    document.querySelector('#win').play();
                else
                    document.querySelector('#loose').play();
            }
            else if(this.coups >= this.l * this.c)
            {
                this.span.textContent = 'Match nul';
                this.game_status = 0;
            }

            if(this.game_status === 0)
            {
                this.box.style.display = 'block';
            }
        },

        win : function(row, column, cname)
        {
            var winners = [];
            for(var i = 0, s = 0; i < this.c; i++)
            {
                s = this.tab[row][i].className === cname ? s+1 : 0;
                if(s >= 4)
                {
                    winners.push(this.tab[row][i]);
                    winners.push(this.tab[row][i-1]);
                    winners.push(this.tab[row][i-2]);
                    winners.push(this.tab[row][i-3]);
                    return winners;
                }
            }

            for(var i = 0, s = 0; i < this.l; i++)
            {
                s = this.tab[i][column].className === cname ? s+1 : 0;
                if(s >= 4)
                {
                    winners.push(this.tab[i][column]);
                    winners.push(this.tab[i-1][column]);
                    winners.push(this.tab[i-2][column]);
                    winners.push(this.tab[i-3][column]);
                    return winners;
                }
            }

            var difRowCol = row-column;
            for(var i = Math.max(difRowCol, 0), s = 0; i < Math.min(this.l, this.c + difRowCol); i++)
            {
                s = this.tab[i][i-difRowCol].className === cname ? s+1 : 0;
                if(s >= 4)
                {
                    winners.push(this.tab[i][i-difRowCol]);
                    winners.push(this.tab[i-1][i-difRowCol-1]);
                    winners.push(this.tab[i-2][i-difRowCol-2]);
                    winners.push(this.tab[i-3][i-difRowCol-3]);
                    return winners;
                }
            }

            var somRowCol = row+column;
            for(var i = Math.max(somRowCol - this.c + 1, 0), s = 0; i < Math.min(this.l, somRowCol + 1); i++)
            {
                s = this.tab[i][somRowCol-i].className === cname ? s+1 : 0;
                if(s >= 4)
                {
                    winners.push(this.tab[i][somRowCol-i]);
                    winners.push(this.tab[i-1][somRowCol-i+1]);
                    winners.push(this.tab[i-2][somRowCol-i+2]);
                    winners.push(this.tab[i-3][somRowCol-i+3]);
                    return winners;
                }
            }
            return false;
        },

        bot : function()
        {
            var colBot = -10;
            colBot = this.analyse('J' + (3-this.turn), colBot);
            var tmp = colBot;
            colBot = this.analyse('J' + (this.turn), colBot);
            if(colBot < 0 && tmp >= 0)
                colBot = tmp;
            if(colBot < 0)
            {
                var hasard = Math.round(Math.random()*(this.c-1));
                colBot = (-colBot)-1;
                if(colBot === hasard)
                    while(colBot === hasard)
                        colBot = Math.round(Math.random()*(this.c-1));
                else
                    colBot = hasard;
            }
            this.play(colBot);
        },

        analyse : function(cname, column)
        {
            for(var i = 0, s = 0; i < this.l; i++)
                for(var j = 0; j < this.c; j++)
                {
                    s = this.tab[i][j].className === cname ? s+1 : 0;
                    if(s === 2)
                    {
                        if(j+2 < this.c && this.tab[i][j+2].className === cname && this.tab[i][j+1].className === '')
                            if(i === 0 || this.tab[i-1][j+1].className)
                                return(j+1);
                            else if (column < 0 && (i === 1 || this.tab[i-2][j+1].className))
                                column = -(j+1) -1;
                        if(j-3 >= 0 && this.tab[i][j-3].className === cname && this.tab[i][j-2].className === '')
                            if(i === 0 || this.tab[i-1][j-2].className)
                                return(j-2);
                            else if (column < 0 && (i === 1 || this.tab[i-2][j-2].className))
                                column = -(j-2) -1;
                        if(column < 0 && j-2 >= 0 && j+1 < this.c)
                        {
                            if(i === 0 || this.tab[i-1][j-2].className)
                                if(this.tab[i][j-2].className === '' && this.tab[i][j+1].className === '')
                                    column = j-2;
                                else if (i === 0 || this.tab[i-1][j+1].className)
                                    if(this.tab[i][j+1].className === '' && this.tab[i][j-2].className === '')
                                        column = j+1;
                        }
                    }
                    if(s === 3)
                    {
                        if(j+1 < this.c && this.tab[i][j+1].className === '')
                            if(i === 0 || this.tab[i-1][j+1].className)
                                return(j+1);
                            else if (column < 0  && (i === 1 || this.tab[i-2][j+1].className))
                                column = -(j+1) -1;
                        if(j-3 >= 0 && this.tab[i][j-3].className === '')
                            if(i === 0 || this.tab[i-1][j-3].className)
                                return(j-3);
                            else if (column < 0 && (i === 1 || this.tab[i-2][j-3].className))
                                column = -(j-3) -1;
                    }
                }
            for(var i = 0, s = 0; i < this.c; i++)
                for(var j = 0; j < this.l; j++)
                {
                    s = this.tab[j][i].className === cname ? s+1 : 0;
                    if(s === 3 && j+1 < this.l && this.tab[j+1][i].className === '')
                        return(i);
                }

            var row = 0, col = this.c-1;

            while(row <= this.l-1)
            {
                var difRowCol = row-col;
                for(var i = Math.max(difRowCol, 0), s = 0; i < Math.min(this.l, this.c + difRowCol); i++)
                {
                    s = this.tab[i][i-difRowCol].className === cname ? s+1 : 0;

                    if(s === 2)
                    {
                        if(i-difRowCol-3 >= 0 && i-3 >= 0 && this.tab[i-2][i-difRowCol-2].className === '' && this.tab[i-3][i-difRowCol-3].className === cname)
                            if(this.tab[i-3][i-difRowCol-2].className)
                                return(i-difRowCol-2);
                            else if (column < 0 && (i === 3 || this.tab[i-4][i-difRowCol-2].className))
                                column = -(i-difRowCol-2) -1;

                        if(i-difRowCol+2 < this.c && i+2 < this.l && this.tab[i+1][i-difRowCol+1].className === '' && this.tab[i+2][i-difRowCol+2].className === cname)
                            if(this.tab[i][i-difRowCol+1].className)
                                return(i-difRowCol+1);
                            else if (column < 0  && this.tab[i-1][i-difRowCol+1].className)
                                column = -(i-difRowCol+1) -1;
                    }
                    if(s === 3)
                    {
                        if(i > 2 && i-difRowCol > 2 && this.tab[i-3][i-difRowCol-3].className === '')
                            if(i-3 === 0 || this.tab[i-4][i-difRowCol-3].className)
                                return(i-difRowCol-3);
                            else if (column < 0 && (i === 4 || this.tab[i-5][i-difRowCol-3].className))
                                column = -(i-difRowCol-3) -1;

                        if(i < this.l-1 && i-difRowCol < this.c-1 && this.tab[i+1][i-difRowCol+1].className === '')
                            if(this.tab[i][i-difRowCol+1].className)
                                return(i-difRowCol+1);
                            else if (column < 0 && this.tab[i-1][i-difRowCol+1].className)
                                column = -(i-difRowCol+1) -1;
                    }
                }
                if(col > 0)
                    col -= 1;
                else
                    row += 1;
            }

            row = this.l-1, col = this.c-1;

            while(col >= 0)
            {
                var somRowCol = row+col;
                for(var i = Math.max(somRowCol - this.c + 1, 0), s = 0; i < Math.min(this.l, somRowCol + 1); i++)
                {
                    s = this.tab[i][somRowCol-i].className === cname ? s+1 : 0;
                    if(s === 2)
                    {
                        if(somRowCol-i+3 < this.c && i-3 >= 0 && this.tab[i-2][somRowCol-i+2].className === '' && this.tab[i-3][somRowCol-i+3].className === cname)
                            if(this.tab[i-3][somRowCol-i+2].className)
                                return(somRowCol-i+2);
                            else if (column < 0 && (i === 3 || this.tab[i-4][somRowCol-i+2].className))
                                column = -(somRowCol-i+2) -1;

                        if(somRowCol-i-2 >= 0 && i+2 < this.l && this.tab[i+1][somRowCol-i-1].className === '' && this.tab[i+2][somRowCol-i-2].className === cname)
                            if(this.tab[i][somRowCol-i-1].className)
                                return(somRowCol-i-1);
                            else if (column < 0 && this.tab[i-1][somRowCol-i-1].className)
                                column = -(somRowCol-i-1) -1;
                    }
                    if(s === 3)
                    {
                        if(i > 2 && somRowCol-i < this.c-3 && this.tab[i-3][somRowCol-i+3].className === '')
                            if(i-3 === 0 || this.tab[i-4][somRowCol-i+3].className)
                                return(somRowCol-i+3);
                            else if (column < 0 && (i === 4 || this.tab[i-5][somRowCol-i+3].className))
                                column = -(somRowCol-i+3) -1;

                        if(i < this.l-1 && somRowCol-i > 0 && this.tab[i+1][somRowCol-i-1].className === '')
                            if(this.tab[i][somRowCol-i-1].className)
                                return(somRowCol-i-1);
                            else if (column < 0 && this.tab[i-1][somRowCol-i-1].className)
                                column = -(somRowCol-i-1) -1;
                    }
                }
                if(row > 0)
                    row -= 1;
                else
                    col -= 1;
            }
            return column;
        },

        reset : function()
        {
            this.coups = 0;
            this.box.style.display = 'none';
            this.game_status = 1;
            this.turn = 1;

            for(var i = 0; i < this.l; i++)
                for(var j = 0; j < this.c; j++)
                    this.tab[i][j].className = '';
        }
    }

p4.init(document.querySelector('#jeu'));